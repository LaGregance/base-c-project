# Base C Project

A project template for C

## How to use it ?
1. Clone the repo
2. [A] if you want to build a binary : use `make binary`
3. [B] if you want to build a library : Use `make lib`
4. Your project is ready to start

-> You can add name="xxx" for set project name

## Config your makefile (for binary)
In "PROJECT" section of Makefile you can configure a lot of things.  
NAME, HEADER_FOLDER, SOURCES_FOLDER, OBJS_FOLDER, CC, CFLAGS, SOURCES (I'm not going to explain to you what this is all about).  
  
There is also something like this :  
```
## Add your libs in this array
##          PATH        INCLUDE PATH      TARGET
LIBRARIES=  libft       libft/includes    libft/libft.a \
            minilib     minilib           minilib/minilib.a
```
It's pretty simple : just add a newline for a new lib and add the path, the include folder and the `.a`.  
  
Other variables :  
`AUTO_HEADER_TARGET`	: The main .h file for export prototype (see below, make headers)  
`MAKEFILE_SCRIPTS_DIR`	: Makefile scripts directory (makefile_utils by default)  
`FLAG_FILE`				: A file for storing the last flags used  
  
That's all, enjoy fast development !  
  
## Use the makefile
`make`	                : build lib and binary  
`make clean/fclean/re`	: no comment  
`make headers`			: generate your .h files with all your functions export from your .c files  
`make gitignore`		: generate a .gitignore with your custom rules in ./makefile_utils/gitignore AND your binary ([!!WARNING!!] this command will erase an old .gitignore)  
`make doc`              : Add a comment block on top of your functions and build the doc with doxygen  
  
Variables :  
`noflags=1`						: make without flags  
`flags="-flags -to -compile"`	: make with these flags  
`addflags="-flags -to -add"`	: make with default flags AND these flags  
  
Shortcut :  
`make sanitize`	: make addflags="-g3 -fsanitize=address"  
`make valgrind`	: make addflags="-g3" AND launch valgrind on your binary  
  
## TODO
- Test Engine in C++