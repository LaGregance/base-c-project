#!/bin/sh

# $1    : Makefile rules
# $2    : target lib (.a)
# $3... : Makefile $(LIBRARIES)

RULE=$1
TARGET_LIB=$2

shift 2
i=0
args=("$@")

let "IS_GOOD_LIB = $# % 3"
if [[ $IS_GOOD_LIB == 0 ]]; then
	while [[ $i < $# ]] ; do
		let "i_lib = $i + 2"
		if [[ "${args[$i_lib]}" == "$TARGET_LIB" ]]; then
			make -C ${args[$i]} $RULE
		fi
		let "i = i + 3"
	done
fi