#!/bin/sh

# $1    : FLAG_FILE
# $2... : flags

FLAG_FILE=$1
shift 1

if [ -f $FLAG_FILE ]; then
	LAST_FLAGS=$(cat $FLAG_FILE)
	if [[ "$@" != "$LAST_FLAGS" ]]; then
		echo "$@" > $FLAG_FILE
	fi
else
	echo "$@" > $FLAG_FILE
fi
