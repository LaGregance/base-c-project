#!/bin/sh

# $1 : source dir

BASEDIR=$(dirname "$0")

process_folder()
{
	# $1 : start dir (./srcs/* and subdirs)

	for f in "$@"
	do
		if [[ $f == *.c ]]
		then
			# Lists all prototypes from file $f and adds them in $AUTOGEN_FILE (without static functions)
			sh ${BASEDIR}/gen_doc.sh $f
		elif [ -d "$f" ]
		then
			process_folder $f/*
		fi
	done
}

process_folder $1