#!/bin/sh

##############################
########### CONFIG ###########
##############################
AUTO_SUBFOLDER=auto
##############################

# $1 : auto_header.h main target
# $2 : start dir (./srcs)

HEADER_FOLDER=$(dirname $1)
AUTO_FOLDER=$HEADER_FOLDER/$AUTO_SUBFOLDER
MAIN_AUTOGEN_FILE=$1

get_auto_header_name()
{
	# $1 : dir
	
	# Remove "./" and replace '/' by '_'
	GEN_NAME=${1//.\//}
	GEN_NAME=${GEN_NAME//\//_}
	HEADER_TARGET=auto_${GEN_NAME}.h

	echo $AUTO_FOLDER/$HEADER_TARGET
}

process_folder()
{
	# $1 : auto_header.h target
	# $2 : start dir (./srcs/* and subdirs)

	AUTOGEN_FILE=$1
	shift 1
	for f in "$@"
	do
		if [[ $f == *.c ]]
		then
			# Lists all prototypes from file $f and adds them in $AUTOGEN_FILE (without static functions)
			ctags -x $f | sed -E "s~.+${f:2}[ \t]+~~g" | grep -v "^static" | sed 's/$/;/' >> $AUTOGEN_FILE
		fi
	done
	for f in "$@"
	do
		if [ -d "$f" ]
		then
			HEADER_TARGET=$(get_auto_header_name $f)
			process_file $HEADER_TARGET $f
		fi
	done
}

process_file()
{
	# $1 : auto_header.h target
	# $2 : start dir (./srcs)

	# Add include of subfile in the main header file
	INCLUDE_FILE=${1//$HEADER_FOLDER\//}
	echo "#include \"$INCLUDE_FILE\"" >> $MAIN_AUTOGEN_FILE

	# Added protection for multipe inclusion
	DEFINE_PROTECT=$(basename "$1" | sed -e 's/\./_/g' | tr '[a-z]' '[A-Z]')
	echo "#ifndef $DEFINE_PROTECT" > $1
	echo "#define $DEFINE_PROTECT" >> $1
	echo "" >> $1
	process_folder $1 $2/*
	echo "" >> $1
	echo "#endif" >> $1
}

mkdir -p $AUTO_FOLDER

DEFINE_PROTECT=$(basename "$MAIN_AUTOGEN_FILE" | sed -e 's/\./_/g' | tr '[a-z]' '[A-Z]')
echo "#ifndef $DEFINE_PROTECT" > $MAIN_AUTOGEN_FILE
echo "#define $DEFINE_PROTECT" >> $MAIN_AUTOGEN_FILE
echo "" >> $MAIN_AUTOGEN_FILE

HEADER_TARGET=$(get_auto_header_name $2)
process_file $HEADER_TARGET $2

echo "" >> $MAIN_AUTOGEN_FILE
echo "#endif" >> $MAIN_AUTOGEN_FILE