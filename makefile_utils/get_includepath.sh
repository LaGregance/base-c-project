#!/bin/sh

# $@ : Makefile $(LIBRARIES)

i=1
args=("$@")
let "IS_GOOD_LIB = $# % 3"

if [[ $IS_GOOD_LIB == 0 ]]; then
	while [[ $i < $# ]] ; do
		printf " -I"
 		printf "${args[$i]}"
		let "i = i + 3"
	done
	printf "\n"
fi