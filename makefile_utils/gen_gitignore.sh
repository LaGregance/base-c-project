#!/bin/sh

##############################
########### CONFIG ###########
##############################
BASEDIR=$(dirname "$0")
GITIGNORE_BASE=${BASEDIR}/gitignore
##############################

# $1    : Makefile $(NAME)
# $2    : Makefile $(OBJS_FOLDER)
# $3... : Makefile $(LIBRARIES)

NAME=$1
OBJS_FOLDER=$2
shift 2

if [ -f $GITIGNORE_BASE ]; then
	GITIGNORE_CONTENT=$(cat $GITIGNORE_BASE)

	i=2
	args=("$@")

	echo "# Custom rules" > .gitignore
	echo "$GITIGNORE_CONTENT" >> .gitignore
	echo "" >> .gitignore

	echo "# Program rules" >> .gitignore
	echo "$NAME" >> .gitignore
	echo "$OBJS_FOLDER" >> .gitignore
	echo "" >> .gitignore

	let "IS_GOOD_LIB = $# % 3"
	if [[ $IS_GOOD_LIB == 0 ]]; then
		echo "# Libraries rules" >> .gitignore
		while [[ $i < $# ]] ; do
			echo "${args[$i]}" >> .gitignore
			let "i = i + 3"
		done
	fi
fi