#!/bin/sh

# $@ : Makefile $(LIBRARIES)

i=2
args=("$@")
let "IS_GOOD_LIB = $# % 3"

if [[ $IS_GOOD_LIB == 0 ]]; then
	while [[ $i < $# ]] ; do
		printf " "
 		printf "${args[$i]}"
		let "i = i + 3"
	done
	printf "\n"
fi