#!/bin/sh

# $1    : Makefile rules
# $2... : Makefile $(LIBRARIES)

RULE=$1

shift 1
i=0
args=("$@")

let "IS_GOOD_LIB = $# % 3"
if [[ $IS_GOOD_LIB == 0 ]]; then
	while [[ $i < $# ]] ; do
		make -C ${args[$i]} $RULE
		let "i = i + 3"
	done
fi