#!/bin/sh

# $1  : file to doc

REGEX_LINE='.+ ([0-9]+) [^ ]+ (.+)'
REGEX_PROTO='(.+) (.+)\((.*)\)'
REGEX_ARGS=' ?.+ (.+) ?'

FILE=$1
TMP_FILE=./output.c
cp $FILE $TMP_FILE

ctags -x $FILE | sort +1 -n -r | while read line ; do\
	if [[ "$line" =~ $REGEX_LINE ]]; then
		i=1

		LINE_NUMBER=${BASH_REMATCH[1]}
		PROTO="$(echo "${BASH_REMATCH[2]}" | sed -e 's/^[[:space:]]*//')"
		PROTO="$(echo "${PROTO}" | sed -E 's/[[:space:]]+/ /g')"

		let "PREV_LINE=$LINE_NUMBER - 1"
		PREV_LINE=$(awk NR==$PREV_LINE $FILE)
		## On documente uniquement si la ligne du dessus est vide
		if [[ $PREV_LINE == "" ]]; then
			if [[ "$PROTO" =~ $REGEX_PROTO ]]; then
				
				echo "Generate comment for function: $PROTO"

				TYPE=${BASH_REMATCH[1]}
				NAME=${BASH_REMATCH[2]}
				ARGS=${BASH_REMATCH[3]}
				ARGS="$(echo "${ARGS}" | sed -e 's/^[[:space:]]*//')"
				ARGS="$(echo "${ARGS}" | sed -E 's/[[:space:]]+/ /g')"
				sed "${LINE_NUMBER}i\\
					/**\\
					\\ * \\
					\\ * @brief 
					 " $TMP_FILE > tmp
				mv tmp $TMP_FILE
				let "LINE_NUMBER=$LINE_NUMBER + 3"
				
				echo $ARGS | tr "," "\n" > ARGS_TMP_FILE
				while read param ; do
					if [[ "$param" =~ $REGEX_ARGS ]]; then
						ARG_NAME="$(echo "${BASH_REMATCH[1]}" | sed -e 's/\*//g')"

						sed "${LINE_NUMBER}i\\
							\\ * @param ${ARG_NAME}
							 " $TMP_FILE > tmp
						mv tmp $TMP_FILE
						let "LINE_NUMBER=$LINE_NUMBER + 1"
					fi
				done < ARGS_TMP_FILE
				rm ARGS_TMP_FILE

				if [[ $TYPE != 'void' ]]; then
					sed "${LINE_NUMBER}i\\
							\\ * @return 
							" $TMP_FILE > tmp
					mv tmp $TMP_FILE
					let "LINE_NUMBER=$LINE_NUMBER + 1"
				fi
				sed "${LINE_NUMBER}i\\
					\\ */
					" $TMP_FILE > tmp
				mv tmp $TMP_FILE
			fi
		fi
	fi
done

mv $TMP_FILE $FILE