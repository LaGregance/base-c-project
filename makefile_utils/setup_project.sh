#!/bin/sh

# $1 : $(NAME)

mkdir srcs
mkdir includes

rm Makefile
cp templates/Makefile Makefile
cp templates/Doxyfile Doxyfile
#Warning : Maybe this doesn't work on linux
sed -i "" "s/\[\[NAME\]\]/$1/g" Makefile
if [[ "$1" == *".a" ]]; then
	echo "Lib generated !"
else
	cp templates/main.c srcs/main.c
	echo "Project generated !"
fi
rm -rf templates
rm makefile_utils/setup_project.sh