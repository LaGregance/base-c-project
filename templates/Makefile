#########################################################################################
####################################### PROJECT #########################################
#########################################################################################
NAME=[[NAME]]

HEADER_FOLDER=./includes
SOURCES_FOLDER=./srcs
OBJS_FOLDER=./bin

CC=gcc
CFLAGS=-Wall -Wextra -Werror

SOURCES=$(shell find $(SOURCES_FOLDER) -type f -name '*.c')

## Add your libs in this array
##			PATH			INCLUDE PATH		TARGET
LIBRARIES=

AUTO_HEADER_TARGET=$(HEADER_FOLDER)/auto_header.h
MAKEFILE_SCRIPTS_DIR=./makefile_utils
FLAG_FILE=$(MAKEFILE_SCRIPTS_DIR)/last_flags

#########################################################################################
################################### CUSTOMS RULES #######################################
#########################################################################################

ifdef flags
	CFLAGS=$(flags)
else ifdef noflags
	CFLAGS=
else ifdef addflags
	CFLAGS+=$(addflags)
endif

#########################################################################################
##################################### VARIABLES #########################################
#########################################################################################

INCLUDES=$(shell find $(HEADER_FOLDER) -type f -name '*.h')
OBJS=$(SOURCES:$(SOURCES_FOLDER)%.c=$(OBJS_FOLDER)%.o)
OBJS_DIRS = $(shell find $(SOURCES_FOLDER) -type d -exec echo {} \; | sed 's~$(SOURCES_FOLDER)~$(OBJS_FOLDER)~g')

COUNT_LIB=$(shell echo "$(words $(LIBRARIES)) / 3" | bc)
INCLUDE_PATH = -I $(HEADER_FOLDER)
INCLUDE_PATH += $(shell sh ./$(MAKEFILE_SCRIPTS_DIR)/get_includepath.sh $(LIBRARIES))
TARGET_LIBS = $(shell sh ./$(MAKEFILE_SCRIPTS_DIR)/get_targetlibs.sh $(LIBRARIES))


#########################################################################################
####################################### RULES ###########################################
#########################################################################################
all:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/relink_flags_differ.sh $(FLAG_FILE) $(CFLAGS)
	@make $(NAME)

ifeq ($(patsubst %.a,,$(lastword $(NAME))),)
$(NAME): $(TARGET_LIBS) $(OBJS_FOLDER) $(OBJS)
	ar rc $(NAME) $(OBJS)
	ranlib $(NAME)
else
$(NAME): $(TARGET_LIBS) $(OBJS_FOLDER) $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS)
endif

$(TARGET_LIBS): FORCE
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/make_lib.sh " " $@ $(LIBRARIES)

$(OBJS_FOLDER):
	@mkdir -p $(OBJS_FOLDER) $(OBJS_DIRS)

$(OBJS_FOLDER)/%.o: $(SOURCES_FOLDER)/%.c Makefile $(INCLUDES) $(FLAG_FILE)
	$(CC) $(CFLAGS) $(INCLUDE_PATH) -o $@ -c $<

clean:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/make_libs.sh "clean" $(LIBRARIES)
	rm -rf $(OBJS_FOLDER)

fclean:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/make_libs.sh "fclean" $(LIBRARIES)
	rm -rf $(OBJS_FOLDER)
	rm -f $(NAME)

re: fclean all

headers:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/gen_headers.sh $(AUTO_HEADER_TARGET) $(SOURCES_FOLDER)

gitignore:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/gen_gitignore.sh $(NAME) $(OBJS_FOLDER) $(LIBRARIES)

doc:
	@sh ./$(MAKEFILE_SCRIPTS_DIR)/gen_docs.sh $(SOURCES_FOLDER)
	doxygen

## SHORTCUT
valgrind:
	@make addflags="-g3"
	valgrind --leak-check=full ./$(NAME)

sanitize:
	@make addflags="-g3 -fsanitize=address"
## --------

FORCE:

.PHONY: all clean fclean re headers valgrind sanitize doc
