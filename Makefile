MAKEFILE_SCRIPTS_DIR=./makefile_utils


all:
	@echo "Usage: make binary OR make lib"

binary:
ifndef name
	$(eval name=a.out)
endif
	@sh $(MAKEFILE_SCRIPTS_DIR)/setup_project.sh $(name)

lib:
ifndef name
	$(eval name=lib.a)
endif
	@sh $(MAKEFILE_SCRIPTS_DIR)/setup_project.sh $(name)

.PHONY: all binary lib